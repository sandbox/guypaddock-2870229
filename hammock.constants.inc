<?php
/**
 * @file
 *   Constants for the the "Hammock" module.
 *
 *   <p>© 2014-2017 Red Bottle Design, LLC and Inveniem, LLC.
 *   All rights reserved.</p>
 *
 * @author  Guy Paddock (guy@redbottledesign.com)
 */

/**
 * The machine name for this module.
 *
 * @var string
 */
define('HAMMOC_MODULE_NAME', 'hammock');
