# Hammock
This module wraps the [Hamcrest](https://github.com/hamcrest/hamcrest-php) and
[Mockery](https://github.com/mockery/mockery) PHP testing libraries using the
[Libraries API](https://www.drupal.org/project/libraries).

## Requirements
- The following modules:
  - [Libraries API](https://www.drupal.org/project/libraries)
  - [X Autoload](https://www.drupal.org/project/xautoload)
- The following libraries:
  - [Hamcrest](https://github.com/hamcrest/hamcrest-php) installed so that
    `composer.json` and the `hamcrest` folder, from the
    [ZIP file](https://github.com/hamcrest/hamcrest-php/releases), are located
    at `sites/[SITE NAME]/libraries/hamcrest/composer.json` and
    `sites/[SITE NAME]/libraries/hamcrest/hamcrest` respectively.
  - [Mockery](https://github.com/mockery/mockery) installed so that
    `composer.json` and the `library` folder, from the
    [ZIP file](https://github.com/mockery/mockery/releases), are located at
    `sites/[SITE NAME]/libraries/mockery/composer.json` and 
    `sites/[SITE NAME]/libraries/mockery/library` respectively.
  - [Super Closure](https://github.com/jeremeamia/super_closure) *(not strictly
    required, but highly encouraged)* installed so
    that `composer.json` and the `src` folder, from the
    [ZIP file](https://github.com/jeremeamia/super_closure/releases), are
    located at `sites/[SITE NAME]/libraries/super-closure/composer.json` and 
    `sites/[SITE NAME]/libraries/super-closure/src` respectively. You will also
    need to download and install the following dependencies of Super Closure:
    - [PHP Parser](https://github.com/nikic/PHP-Parser), version 3.0.5,
      installed so that `composer.json` and the `lib` folder, from the
      [ZIP file](https://github.com/nikic/PHP-Parser/releases/tag/v3.0.5), are
      located at `sites/[SITE NAME]/libraries/nikic-php-parser/composer.json`
      and `sites/[SITE NAME]/libraries/nikic-php-parser/lib` respectively.
    - [Symfony Polyfill / Util](https://github.com/symfony/polyfill-util),
      version 1.4.0, installed so that `composer.json` and `Binary.php`, from
      the
      [ZIP file](https://github.com/symfony/polyfill-util/releases/tag/v1.4.0),
      are located at
      `sites/[SITE NAME]/libraries/symfony-polyfill-util/composer.json` and
      `sites/[SITE NAME]/libraries/symfony-polyfill-util/Binary.php`
      respectively.
    - [Symfony Polyfill / Php56](https://github.com/symfony/polyfill-php56),
      version 1.4.0, installed so that `composer.json` and `Php56.php`, from the
      [ZIP file](https://github.com/symfony/polyfill-php56/releases/tag/v1.4.0),
      are located at
      `sites/[SITE NAME]/libraries/symfony-polyfill-php56/composer.json` and
      `sites/[SITE NAME]/libraries/symfony-polyfill-php56/Php56.php`
      respectively.
    

## Complementary Modules and Patches
This module was written to complement & extend the 
[Mockery sandbox module](https://www.drupal.org/sandbox/znerol/1997136) for use
with Drupal SimpleTest, although this module does not strictly depend on that
module. By using the Libraries API, this module frees you from having to use
Composer; you will need to download the libraries manually.

If using this module with the Mockery module, you will want to apply the
following patches:

- [#1525484: Libaries API - Make libraries available at boot](https://www.drupal.org/node/1525484)
  so that Mockery and Hamcrest can load via Libraries during site boot.
- [#2887188: Drupal Core - module_implements_alter() doesn't actually affect hook_boot](https://www.drupal.org/node/2887188)
  so that Mockery is loaded at the right time in the Drupal bootstrap.

If you are having any difficulty with loading of Mockery, Hamcrest, or
Super Closure; or, if you're looking for better support for real-time trace
logging, in general, when running tests, we encourage you to install the
["Test Logging that Doesn't Suck (TL;DS)"](https://www.drupal.org/node/2887069)
logging module and add it to the list of modules enabled in your tests.

## Installation and Usage
1. Install the modules and libraries as described under the Requirements section
   above.
2. When crafting [SimpleTest test cases](https://www.drupal.org/docs/7/testing/simpletest-testing-tutorial-drupal-7),
   ensure that `hammock` is one of the modules you enable
   [during the test set-up](http://cgit.drupalcode.org/examples/tree/simpletest_example/simpletest_example.test?h=7.x-1.x#n55).
3. Follow Mockery's [Getting Started guide](http://docs.mockery.io/en/latest/getting_started/simple_example.html)
   for how to set-up and tear-down mocks.
