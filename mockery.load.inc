<?php
/**
 * @file
 *   Custom loader for the Mockery Sandbox project.
 *
 *   Allows us to use the versions of Mockery and Hamcrest that've been
 *   loaded via the Libraries API.
 *
 *   © 2014-2017 Red Bottle Design, LLC and Inveniem, LLC. All rights reserved.
 *
 *   NOTE: This file can be debugged by installing the TL;DS logger module, and
 *   patching core to fix DDO-2887188.
 */
// Initialize the Libraries API. Requires the patch from DDO-1525484.
if (!function_exists('libraries_load')) {
  drupal_load('module', 'libraries');
}

// Safeguard against missing patch from DDO-1525484
if (!function_exists('_libraries_ensure_common_funcs_loaded')) {
  if (function_exists('tlds_log')) {
    tlds_log('Patch for DDO-1525484 is missing.');
  }
}
else {
  _hammock_load_libraries();
}

/**
 * Returns the list of all of the libraries we need to load.
 */
function _hammock_get_library_list() {
  // NOTE: This mimics some of the info in hammock_libraries_info() because we
  // need to sanity check that all the required libraries are available at
  // BOOT time.
  $libraries = array(
    'mockery' => array(
      'name'    => 'Mockery',
      'classes' => array(
        '\Mockery',
        '\Mockery\Mock',
      ),
    ),
    'hamcrest' => array(
      'name'      => 'Hamcrest',
      'functions' => array(
        'anything',
      ),
      'classes' => array(
        '\Hamcrest\Core\IsAnything',
      ),
    ),
    'nikic-php-parser' => array(
      'name'    => 'PHP Parser',
      'classes' => array(
        '\PhpParser\Parser',
      ),
    ),
    'symfony-polyfill-util' => array(
      'name'    => 'Symfony Polyfill / Util',
      'classes' => array(
        '\Symfony\Polyfill\Util\Binary',
      ),
    ),
    'symfony-polyfill-php56' => array(
      'name'    => 'Symfony Polyfill / Php56',
      'classes' => array(
        '\Symfony\Polyfill\Php56\Php56',
      ),
    ),
    'super-closure' => array(
      'name'    => 'Super Closure',
      'classes' => array(
        '\SuperClosure\Serializer',
      ),
    ),
  );

  return $libraries;
}

/**
 * Invokes the Libraries API and XAutoload to load all of the require libraries
 * for mocking / testing.
 */
function _hammock_load_libraries() {
  // TODO: Find a different way to get XAutoload to handle class loading at boot
  //       time. This causes problems for us after boot (see hammock_init()).
  xautoload()->phaseControl->enterMainPhase();

  $libraries = _hammock_get_library_list();

  foreach ($libraries as $library_name => $library_info) {
    $library_status = libraries_load($library_name);

    _hammock_log_library_status($library_status, $library_info);
  }
}

/**
 * Logs the status of a library using TL;DS (if it is available).
 *
 * @param mixed $library_status
 *   The information returned by about the library from libraries_load().
 * @param array $library_info
 *   The information we can use to verify the library was loaded (from
 *   _hammock_get_library_list()).
 */
function _hammock_log_library_status($library_status, array $library_info) {
  if (function_exists('tlds_log')) {
    $loaded = TRUE;

    if (empty($library_status) || empty($library_status['loaded'])) {
      $loaded = FALSE;
    }

    if ($loaded && isset($library_info['functions'])) {
      foreach ($library_info['functions'] as $function_name) {
        if (!function_exists($function_name)) {
          $loaded = FALSE;
          break;
        }
      }
    }

    if ($loaded && isset($library_info['classes'])) {
      foreach ($library_info['classes'] as $class_name) {
        if (!class_exists($class_name) && !interface_exists($class_name)) {
          $loaded = FALSE;
          break;
        }
      }
    }

    $name = $library_info['name'];

    if ($loaded) {
      tlds_log("{$name} library successfully loaded.");
    }
    else {
      tlds_log("{$name} library did not load.");
    }
  }
}
